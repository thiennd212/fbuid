﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facebook;
using Newtonsoft;
using System.Net;
using Newtonsoft.Json;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Security.Cryptography;

namespace FBUID.Common
{
    /// <summary>
    /// CÁC HÀM DÙNG CHUNG
    /// </summary>
    public static class CommonClass
    {
        /// <summary>
        /// Converts a List to a datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dt = new DataTable();
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor property = properties[i];
                dt.Columns.Add(property.Name, property.PropertyType);
            }
            object[] values = new object[properties.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = properties[i].GetValue(item);
                }
                dt.Rows.Add(values);
            }
            return dt;
        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }


    #region facebook model
    /// <summary>
    /// Facebook model for return data
    /// </summary>
    public class FacebookModel
    {
        public int No { get; set; }
        //public string FacebookId { get; set; }
        public string FacebookName { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        //public string Phone { get; set; }
        public string Address { get; set; }
    }

    public class ConvertModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("birthday")]
        public string Birthday { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("location")]
        public Location Location { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
    }

    public class Location
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
    public class Data
    {
        public IEnumerable<ConvertModel> data { get; set; }
    }
    #endregion

    public class InputGetToken
    {
        public string api_key { get; set; }
        public string email { get; set; }
        public string format { get; set; }
        public string locale { get; set; }
        public string method { get; set; }
        public string password { get; set; }
        public string return_ssl_resources { get; set; }
        public string v { get; set; }
    }

    public class OutputGetToken
    {
        public string session_key { get; set; }
        public string uid { get; set; }
        public string access_token { get; set; }
        public string machine_id { get; set; }
        public string confirmed { get; set; }
        public string identifier { get; set; }
    }
}
