﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.Common
{
    /// <summary>
    /// ĐỊNH NGHĨA CÁC HẰNG SỐ TẠI ĐÂY
    /// </summary>
    public static class Constant
    {
        /// <summary>
        /// Id của ứng dụng tạo trên FB
        /// </summary>
        public const string APP_ID = "1668730023197211";
        /// <summary>
        /// Secrect của ứng dụng tạo trên FB
        /// </summary>
        public const string SECRECT_KEY = "16a8b3b019703fbe02580e1323308dda";
        /// <summary>
        /// Sign key dùng để lấy token full quyền
        /// </summary>
        public const string SIGN_KEY = "c1e620fa708a1d5696fb991c1bde5662";
        /// <summary>
        /// API key dùng để lấy token full quyền
        /// </summary>
        public const string API_KEY = "3e7c78e35a76a9299309885393b02d97";
        /// <summary>
        /// Url api facebook        
        /// </summary>
        public const string FB_API = "https://graph.facebook.com/v2.3/";
        /// <summary>
        /// Hằng số định nghĩa quét danh sách bạn bè theo 1 UID
        /// </summary>
        public const string SCAN_1_UID = "1";
        /// <summary>
        /// Hằng số định nghĩa quét danh sách bạn bè theo danh sách UID từ tệp
        /// </summary>
        public const string SCAN_FILE_UID = "2";
        /// <summary>
        /// Hằng số định nghĩa menu quét được chọn
        /// </summary>
        public const int QUET_DANH_SACH_BAN = 1;
        public const int QUET_THANH_VIEN_NHOM = 2;
        public const int QUET_LIKE = 3;
        public const int QUET_COMMENT = 4;
        public const int QUET_BAI_VIET = 5;
        public const int QUET_LIKE_PAGE = 6; 
    }
}
