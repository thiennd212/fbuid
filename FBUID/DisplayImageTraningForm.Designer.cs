﻿namespace FBUID
{
    partial class frmDisplayImageTraning
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDisplayImageTraning));
            this.pbImage1 = new System.Windows.Forms.PictureBox();
            this.pbImage2 = new System.Windows.Forms.PictureBox();
            this.pbImage3 = new System.Windows.Forms.PictureBox();
            this.pbImage6 = new System.Windows.Forms.PictureBox();
            this.pbImage5 = new System.Windows.Forms.PictureBox();
            this.pbImage4 = new System.Windows.Forms.PictureBox();
            this.pbImage7 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage7)).BeginInit();
            this.SuspendLayout();
            // 
            // pbImage1
            // 
            this.pbImage1.Image = global::FBUID.Properties.Resources.buoc1;
            this.pbImage1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pbImage1.InitialImage")));
            this.pbImage1.Location = new System.Drawing.Point(51, 38);
            this.pbImage1.Name = "pbImage1";
            this.pbImage1.Size = new System.Drawing.Size(205, 80);
            this.pbImage1.TabIndex = 0;
            this.pbImage1.TabStop = false;
            // 
            // pbImage2
            // 
            this.pbImage2.Location = new System.Drawing.Point(296, 38);
            this.pbImage2.Name = "pbImage2";
            this.pbImage2.Size = new System.Drawing.Size(205, 80);
            this.pbImage2.TabIndex = 1;
            this.pbImage2.TabStop = false;
            // 
            // pbImage3
            // 
            this.pbImage3.Location = new System.Drawing.Point(539, 38);
            this.pbImage3.Name = "pbImage3";
            this.pbImage3.Size = new System.Drawing.Size(205, 80);
            this.pbImage3.TabIndex = 2;
            this.pbImage3.TabStop = false;
            // 
            // pbImage6
            // 
            this.pbImage6.Location = new System.Drawing.Point(540, 138);
            this.pbImage6.Name = "pbImage6";
            this.pbImage6.Size = new System.Drawing.Size(205, 80);
            this.pbImage6.TabIndex = 5;
            this.pbImage6.TabStop = false;
            // 
            // pbImage5
            // 
            this.pbImage5.Location = new System.Drawing.Point(297, 138);
            this.pbImage5.Name = "pbImage5";
            this.pbImage5.Size = new System.Drawing.Size(205, 80);
            this.pbImage5.TabIndex = 4;
            this.pbImage5.TabStop = false;
            // 
            // pbImage4
            // 
            this.pbImage4.Location = new System.Drawing.Point(52, 138);
            this.pbImage4.Name = "pbImage4";
            this.pbImage4.Size = new System.Drawing.Size(205, 80);
            this.pbImage4.TabIndex = 3;
            this.pbImage4.TabStop = false;
            // 
            // pbImage7
            // 
            this.pbImage7.Location = new System.Drawing.Point(52, 237);
            this.pbImage7.Name = "pbImage7";
            this.pbImage7.Size = new System.Drawing.Size(205, 80);
            this.pbImage7.TabIndex = 6;
            this.pbImage7.TabStop = false;
            // 
            // frmDisplayImageTraning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 344);
            this.Controls.Add(this.pbImage7);
            this.Controls.Add(this.pbImage6);
            this.Controls.Add(this.pbImage5);
            this.Controls.Add(this.pbImage4);
            this.Controls.Add(this.pbImage3);
            this.Controls.Add(this.pbImage2);
            this.Controls.Add(this.pbImage1);
            this.Name = "frmDisplayImageTraning";
            this.Text = "DisplayImageTraningForm";
            ((System.ComponentModel.ISupportInitialize)(this.pbImage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImage7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbImage1;
        private System.Windows.Forms.PictureBox pbImage2;
        private System.Windows.Forms.PictureBox pbImage3;
        private System.Windows.Forms.PictureBox pbImage6;
        private System.Windows.Forms.PictureBox pbImage5;
        private System.Windows.Forms.PictureBox pbImage4;
        private System.Windows.Forms.PictureBox pbImage7;
    }
}