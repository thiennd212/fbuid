﻿using FBUID.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.MeCommentPost
{
    public class CommentPostHandler
    {
        /// <summary>
        /// Hàm quét danh sách thành viên nhóm theo 1 hoặc nhiều GID
        /// </summary>
        /// <param name="lstUID"></param>
        /// <returns></returns>
        public List<ListDisplayGridviewCMT> GetListCommentPost(List<string> lstPID, string token, string idFacebook)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    List<ListDisplayGridviewCMT> lstReturn = new List<ListDisplayGridviewCMT>();
                    List<CommentPostModel> lstCMT = new List<CommentPostModel>();
                    foreach (var item in lstPID)
                    {
                        string accessToken = "?access_token=" + token;
                        string url = Constant.FB_API + idFacebook + "_" + item + "/comments" + accessToken;
                        HttpResponseMessage a = client.GetAsync(url).Result;
                        var b = a.Content.ReadAsStringAsync();
                        CommentModel c = JsonConvert.DeserializeObject<CommentModel>(b.Result);
                        if (c != null)
                        {
                            lstCMT.AddRange(c.Data);
                        }
                    }
                    lstCMT = lstCMT.OrderBy(x => x.CreatedTime).ToList();
                    if (lstCMT != null && lstCMT.Count > 0)
                    {
                        int i = 1;
                        foreach (var item in lstCMT)
                        {
                            string accessToken = "&access_token=" + token;
                            string fields = "id,name,birthday,email,location,mobile_phone,gender";
                            string url = Constant.FB_API + item.from.Id + "?fields=" + fields + accessToken;
                            HttpResponseMessage a = client.GetAsync(url).Result;
                            var b = a.Content.ReadAsStringAsync();
                            try
                            {
                                from c = JsonConvert.DeserializeObject<from>(b.Result);
                                lstReturn.Add(new ListDisplayGridviewCMT()
                                {
                                    STT = i.ToString(),
                                    ID = c.Id,
                                    FBName = c.Name,
                                    Gender = (c.Gender) == "male" ? "Nam" : (c.Gender) == "female" ? "Nữ" : "Không có thông tin",
                                    Birthday = c.Brithday,
                                    Email = c.Email,
                                    Phone = c.Phone,
                                    Address = c.Location.Address,
                                    Message = item.Message,
                                });
                                i++;
                            }
                            catch
                            {
                                continue;
                            }
                        }
                    }
                    return lstReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ListDisplayGridviewCMT> GetListCommentPostKhongTrung(List<ListDisplayGridviewCMT> lstCmtPost)
        {
            try
            {
                List<ListDisplayGridviewCMT> lstReturn = new List<ListDisplayGridviewCMT>();
                List<ListDisplayGridviewCMT> lstKhongTrung = new List<ListDisplayGridviewCMT>();
                ListDisplayGridviewCMT info = new ListDisplayGridviewCMT();
                List<string> name = new List<string>();
                name = lstCmtPost.Select(x => x.FBName).Distinct().ToList();
                foreach (var item in name)
                {
                    info = lstCmtPost.Where(x => x.FBName == item).FirstOrDefault();
                    lstKhongTrung.Add(info);
                }
                int i = 1;
                //danh lai so thu tu
                foreach (var item in lstKhongTrung)
                {
                    lstReturn.Add(new ListDisplayGridviewCMT()
                    {
                        STT = i.ToString(),
                        FBName = item.FBName,
                        Gender = item.Gender,
                        Birthday = item.Birthday,
                        Email = item.Email,
                        Phone = item.Phone,
                        Address = item.Address,
                        Message = item.Message,
                    });
                    i++;
                }
                return lstReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ListDisplayGridviewCMT> GetListCommentPostEmail(List<ListDisplayGridviewCMT> lstCmtPost)
        {
            try
            {
                List<ListDisplayGridviewCMT> lstReturn = new List<ListDisplayGridviewCMT>();
                List<ListDisplayGridviewCMT> lstCMTPostEmail = new List<ListDisplayGridviewCMT>();
                ListDisplayGridviewCMT info = new ListDisplayGridviewCMT();
                List<string> name = new List<string>();
                name = lstCmtPost.Select(x => x.FBName).Distinct().ToList();
                foreach (var item in name)
                {
                    info = lstCmtPost.Where(x => x.FBName == item).FirstOrDefault();
                    lstCMTPostEmail.Add(info);
                }
                lstCMTPostEmail = lstCMTPostEmail.Where(x => x.Email != null).ToList();
                //danh lai so thu tu
                int i = 1;
                foreach (var item in lstCMTPostEmail)
                {
                    lstReturn.Add(new ListDisplayGridviewCMT()
                    {
                        STT = i.ToString(),
                        FBName = item.FBName,
                        Gender = item.Gender,
                        Birthday = item.Birthday,
                        Email = item.Email,
                        Phone = item.Phone,
                        Address = item.Address,
                        Message = item.Message,
                    });
                    i++;
                }
                return lstCMTPostEmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
