﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.MeCommentPost
{
    public class CommentModel
    {
        [JsonProperty("data")]
        public List<CommentPostModel> Data { get; set; }
    }
    public class CommentPostModel
    {
        [JsonProperty("created_time")]
        public string CreatedTime { get; set; }
        public from from { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
    }
    public class from 
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("brithday")]
        public string Brithday { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("location")]
        public LocationModel Location { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
    }
    public class ListDisplayGridviewCMT
    {
        public string STT { get; set; }
        public string ID { get; set; }
        public string FBName { get; set; }
        public string Gender { get; set; }
        public string Birthday { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Message { get; set; }
    }
    public class LocationModel
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("name")]
        public string Address { get; set; }
    }
}
