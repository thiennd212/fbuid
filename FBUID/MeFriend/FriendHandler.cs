﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FBUID.Common;
using Newtonsoft.Json;

namespace FBUID.MeFriend
{
    public class FriendHandler
    {
        /// <summary>
        /// Hàm quét danh sách bạn theo 1 hoặc nhiều UID
        /// </summary>
        /// <param name="lstUID"></param>
        /// <returns></returns>
        public List<ListDisplayGridview> GetListFriend(List<string> lstUID, string token)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    List<ListDisplayGridview> lstReturn = new List<ListDisplayGridview>();
                    List<FriendModelList> lstFriends = new List<FriendModelList>();
                    foreach (string uid in lstUID)
                    {
                        string accessToken = "&access_token=" + token;
                        string fields = "friends.limit(10000){name,birthday,email,location,mobile_phone}";
                        string url = Constant.FB_API + "" + uid + "?fields=" + fields + "" + accessToken;
                        HttpResponseMessage a = client.GetAsync(url).Result;
                        var b = a.Content.ReadAsStringAsync();
                        var b2 = b.Result;
                        FriendModelWrap c = JsonConvert.DeserializeObject<FriendModelWrap>(b.Result);
                        if (c != null)
                        {
                            lstFriends.AddRange(c.Friend.Data);
                        }
                    }
                    lstFriends = lstFriends.OrderBy(x => x.FBName).ToList();
                    if (lstFriends != null && lstFriends.Count > 0)
                    {
                        int i = 1;
                        foreach (var item in lstFriends)
                        {
                            lstReturn.Add(new ListDisplayGridview() { STT = i.ToString(), ID = item.ID, FBName = item.FBName, Birthday = item.Birthday, Email = item.Email, Phone = item.Phone, Address = (item.Location != null) ? item.Location.Address : "" });
                            i++;
                        }
                    }
                    return lstReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
