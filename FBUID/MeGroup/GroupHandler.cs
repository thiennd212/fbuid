﻿using FBUID.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.MeGroup
{
    public class GroupHandler
    {
        /// <summary>
        /// Hàm quét danh sách thành viên nhóm theo 1 hoặc nhiều GID
        /// </summary>
        /// <param name="lstUID"></param>
        /// <returns></returns>
        public List<ListDisplayGridviewGroup> GetListMember(List<string> lstUID, string token)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    List<ListDisplayGridviewGroup> lstReturn = new List<ListDisplayGridviewGroup>();
                    List<GroupModelList> lsGroups = new List<GroupModelList>();
                    foreach (string uid in lstUID)
                    {
                        string accessToken = "&access_token=" + token;
                        string fields = "name,birthday,email,location,mobile_phone&limit=30000000";
                        string url = Constant.FB_API + "" + uid + "/members?fields=" + fields + "" + accessToken;
                        HttpResponseMessage a = client.GetAsync(url).Result;
                        var b = a.Content.ReadAsStringAsync();
                        var b2 = b.Result;
                        GroupModel c = JsonConvert.DeserializeObject<GroupModel>(b.Result);
                        if (c != null)
                        {
                            lsGroups.AddRange(c.Data);
                        }
                    }
                    lsGroups = lsGroups.OrderBy(x => x.FBName).ToList();
                    if (lsGroups != null && lsGroups.Count > 0)
                    {
                        int i = 1;
                        foreach (var item in lsGroups)
                        {
                            lstReturn.Add(new ListDisplayGridviewGroup() { STT = i.ToString(), ID = item.ID, FBName = item.FBName, Birthday = item.Birthday, Email = item.Email, Phone = item.Phone, Address = (item.Location != null) ? item.Location.Address : "" });
                            i++;
                        }
                    }
                    return lstReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
