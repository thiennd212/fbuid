﻿using FBUID.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.MeLikePage
{
    public class LikePageHandler
    {
        /// <summary>
        /// Hàm quét danh sách người dùng like page theo 1 hoặc nhiều page id
        /// </summary>
        /// <param name="lstPID"></param>
        /// <returns></returns>
        public List<ListDisplayGridviewLikePage> GetListUserLikePage(List<string> lstPID, string token)
        {
            try
            { 
                using (var client = new HttpClient())
                {
                    List<ListDisplayGridviewLikePage> lstReturn = new List<ListDisplayGridviewLikePage>();
                    List<LikePageModelList> lsGroups = new List<LikePageModelList>();
                    foreach (string pid in lstPID)
                    {
                        string accessToken = "&access_token=" + token;
                        string fields = "name,birthday,email,location,mobile_phone&limit=3000";
                        string url = Constant.FB_API + "" + pid + "/likes?fields=" + fields + "" + accessToken;
                        HttpResponseMessage a = client.GetAsync(url).Result;
                        var b = a.Content.ReadAsStringAsync();
                        var b2 = b.Result;
                        LikePageModel c = JsonConvert.DeserializeObject<LikePageModel>(b.Result);
                        if (c != null)
                        {
                            lsGroups.AddRange(c.Data);
                        }
                    }
                    lsGroups = lsGroups.OrderBy(x => x.FBName).ToList();
                    if (lsGroups != null && lsGroups.Count > 0)
                    {
                        int i = 1;
                        foreach (var item in lsGroups)
                        {
                            lstReturn.Add(new ListDisplayGridviewLikePage() { STT = i.ToString(), ID = item.ID, FBName = item.FBName, Birthday = item.Birthday, Email = item.Email, Phone = item.Phone, Address = (item.Location != null) ? item.Location.Address : "" });
                            i++;
                        }
                    }
                    return lstReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
