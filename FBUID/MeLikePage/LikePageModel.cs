﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.MeLikePage
{
    public class LikePageModel
    {
        [JsonProperty("data")]
        public List<LikePageModelList> Data { get; set; }
    }

    public class LikePageModelList
    {
        [JsonProperty("name")]
        public string FBName { get; set; }
        [JsonProperty("birthday")]
        public string Birthday { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("location")]
        public LocationModel Location { get; set; }
        [JsonProperty("mobile_phone")]
        public string Phone { get; set; }
        [JsonProperty("id")]
        public string ID { get; set; }
    }

    public class LocationModel
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("name")]
        public string Address { get; set; }
    }

    public class ListDisplayGridviewLikePage
    {
        public string STT { get; set; }
        public string ID { get; set; }
        public string FBName { get; set; }
        public string Birthday { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; } 
    }
}
