﻿using FBUID.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.MeLikePost
{
    public class LikeHandler
    {
        /// <summary>
        /// Hàm quét danh sách thành viên nhóm theo 1 hoặc nhiều GID
        /// ID được định nghĩa dưới dạng FBID_POSTID
        /// </summary>
        /// <param name="lstID"></param>
        /// <returns></returns>
        public List<ListDisplayGridviewLike> GetListUserLikesPost(List<string> lstID, string token) 
        {
            try
            {
                using (var client = new HttpClient())
                {
                    List<ListDisplayGridviewLike> lstReturn = new List<ListDisplayGridviewLike>();
                    List<LikeModelList> lsLikes = new List<LikeModelList>();
                    foreach (string id in lstID)
                    {
                        string accessToken = "&access_token=" + token;
                        string fields = "id,name,profile_type&limit=10000";
                        string url = Constant.FB_API + "" + id + "/likes?fields=" + fields + "" + accessToken;
                        HttpResponseMessage a = client.GetAsync(url).Result;
                        var b = a.Content.ReadAsStringAsync();
                        var b2 = b.Result;
                        LikeModel c = JsonConvert.DeserializeObject<LikeModel>(b.Result);
                        if (c != null)
                        {
                            lsLikes.AddRange(c.Data);
                        }
                    }
                    lsLikes = lsLikes.OrderBy(x => x.FBName).ToList();
                    if (lsLikes != null && lsLikes.Count > 0)
                    {
                        int i = 1;
                        foreach (var item in lsLikes)
                        {
                            //lstReturn.Add(new ListDisplayGridviewLike() { STT = i.ToString(), ID = item.ID, FBName = item.FBName, Birthday = item.Birthday, Email = item.Email, Phone = item.Phone, Address = (item.Location != null) ? item.Location.Address : "" });
                            lstReturn.Add(new ListDisplayGridviewLike() { STT = i.ToString(), ID = item.ID, FBName = item.FBName, Type = item.Type });
                            i++;
                        }
                    }
                    return lstReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
