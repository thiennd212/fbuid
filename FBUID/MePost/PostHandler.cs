﻿using FBUID.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.MePost
{
    public class PostHandler
    {
        /// <summary>
        /// Hàm quét danh sách bài viết theo 1 hoặc nhiều GID
        /// </summary>
        /// <param name="lstID"></param>
        /// <returns></returns>
        public List<ListDisplayGridviewPost> GetListUserPost(List<string> lstID, string token, string limit)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    List<ListDisplayGridviewPost> lstReturn = new List<ListDisplayGridviewPost>();
                    List<PostModelList> lstPost = new List<PostModelList>();
                    foreach (string id in lstID)
                    {
                        string accessToken = "&access_token=" + token;
                        string fields = "id,message,likes.limit(3000),shares,comments.limit(3000),created_time&limit=" + limit;
                        string url = Constant.FB_API + "" + id + "/posts?fields=" + fields + "" + accessToken;
                        HttpResponseMessage a = client.GetAsync(url).Result;
                        var b = a.Content.ReadAsStringAsync();
                        var b2 = b.Result;
                        PostModel c = JsonConvert.DeserializeObject<PostModel>(b.Result);
                        if (c != null)
                        {
                            lstPost.AddRange(c.Data);
                        }
                    }
                    lstPost = lstPost.OrderByDescending(x => x.CreateTime).ToList();
                    if (lstPost != null && lstPost.Count > 0)
                    {
                        int i = 1;
                        foreach (var item in lstPost)
                        {
                            //lstReturn.Add(new ListDisplayGridviewPost() { STT = i.ToString(), ID = item.ID, FBName = item.FBName, Birthday = item.Birthday, Email = item.Email, Phone = item.Phone, Address = (item.Location != null) ? item.Location.Address : "" });
                            int totalLike = 0;
                            if (item.Likes != null)
                                totalLike = item.Likes.Data.Count;

                            int totalComment = 0;
                            if (item.Comments != null)
                                totalComment = item.Comments.Data.Count;

                            int totalShare = 0;
                            if (item.Shares != null)
                                totalShare = item.Shares.Count != "" ? Convert.ToInt32(item.Shares.Count) : 0;

                            lstReturn.Add(new ListDisplayGridviewPost() { STT = i.ToString(), ID = item.ID, Message = item.Message, Likes = totalLike, Shares = totalShare, Comments = totalComment });
                            i++;
                        }
                    }
                    return lstReturn;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
