﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FBUID.MePost
{
    public class PostModel
    {
        [JsonProperty("data")]
        public List<PostModelList> Data { get; set; }
    }

    public class PostModelList
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("likes")]
        public Like Likes { get; set; }
        [JsonProperty("shares")]
        public Share Shares { get; set; }
        [JsonProperty("comments")]
        public Comment Comments { get; set; }
        [JsonProperty("created_time")]
        public DateTime CreateTime { get; set; }
    }

    public class Like
    {
        [JsonProperty("data")]
        public List<LikeModel> Data { get; set; }

    }

    public class LikeModel
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Comment
    {
        [JsonProperty("data")]
        public List<CommentModel> Data { get; set; }
    }

    public class CommentModel
    {
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class Share
    {
        [JsonProperty("count")]
        public string Count { get; set; }
    }

    public class ListDisplayGridviewPost
    {
        public string STT { get; set; } 
        public string ID { get; set; }
        public string Message { get; set; }
        public int Likes { get; set; }
        public int Shares { get; set; }
        public int Comments { get; set; } 
    }
}
