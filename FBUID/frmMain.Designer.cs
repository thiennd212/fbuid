﻿namespace FBUID
{
    partial class frmFBUID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFBUID));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtAccessToken = new System.Windows.Forms.TextBox();
            this.pnMenu = new System.Windows.Forms.Panel();
            this.btnLikePage = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDanhSachBan = new System.Windows.Forms.Button();
            this.btnPost = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnComment = new System.Windows.Forms.Button();
            this.btnLike = new System.Windows.Forms.Button();
            this.btnThanhVien = new System.Windows.Forms.Button();
            this.btnTrangChu = new System.Windows.Forms.Button();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.pnHomeLogin = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.btnQuet1UID = new System.Windows.Forms.Button();
            this.btnQuetTepUID = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.pnQuetDSBanHeader = new System.Windows.Forms.Panel();
            this.btnLuuDSUID = new System.Windows.Forms.Button();
            this.nmGioiHan = new System.Windows.Forms.NumericUpDown();
            this.lblGioihan = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dgDanhSach = new System.Windows.Forms.DataGridView();
            this.txtUID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pnListData = new System.Windows.Forms.Panel();
            this.pnMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.pnHomeLogin.SuspendLayout();
            this.pnQuetDSBanHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmGioiHan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDanhSach)).BeginInit();
            this.pnListData.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.SteelBlue;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnLogin.Location = new System.Drawing.Point(111, 72);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(111, 28);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtAccessToken
            // 
            this.txtAccessToken.Location = new System.Drawing.Point(5, 134);
            this.txtAccessToken.Multiline = true;
            this.txtAccessToken.Name = "txtAccessToken";
            this.txtAccessToken.Size = new System.Drawing.Size(877, 50);
            this.txtAccessToken.TabIndex = 1;
            this.txtAccessToken.Text = "Token hiển thị ở đây...";
            // 
            // pnMenu
            // 
            this.pnMenu.BackColor = System.Drawing.Color.SteelBlue;
            this.pnMenu.Controls.Add(this.btnLikePage);
            this.pnMenu.Controls.Add(this.label3);
            this.pnMenu.Controls.Add(this.btnDanhSachBan);
            this.pnMenu.Controls.Add(this.btnPost);
            this.pnMenu.Controls.Add(this.label2);
            this.pnMenu.Controls.Add(this.pictureBox1);
            this.pnMenu.Controls.Add(this.btnComment);
            this.pnMenu.Controls.Add(this.btnLike);
            this.pnMenu.Controls.Add(this.btnThanhVien);
            this.pnMenu.Controls.Add(this.btnTrangChu);
            this.pnMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnMenu.Location = new System.Drawing.Point(0, 0);
            this.pnMenu.Name = "pnMenu";
            this.pnMenu.Size = new System.Drawing.Size(235, 524);
            this.pnMenu.TabIndex = 2;
            // 
            // btnLikePage
            // 
            this.btnLikePage.Enabled = false;
            this.btnLikePage.FlatAppearance.BorderSize = 0;
            this.btnLikePage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLikePage.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLikePage.ForeColor = System.Drawing.Color.White;
            this.btnLikePage.Image = ((System.Drawing.Image)(resources.GetObject("btnLikePage.Image")));
            this.btnLikePage.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLikePage.Location = new System.Drawing.Point(0, 396);
            this.btnLikePage.Name = "btnLikePage";
            this.btnLikePage.Size = new System.Drawing.Size(235, 54);
            this.btnLikePage.TabIndex = 15;
            this.btnLikePage.Text = "   Quét page";
            this.btnLikePage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLikePage.UseVisualStyleBackColor = true;
            this.btnLikePage.Visible = false;
            this.btnLikePage.Click += new System.EventHandler(this.btnLikePage_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(150, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "UID";
            // 
            // btnDanhSachBan
            // 
            this.btnDanhSachBan.Enabled = false;
            this.btnDanhSachBan.FlatAppearance.BorderSize = 0;
            this.btnDanhSachBan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDanhSachBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDanhSachBan.ForeColor = System.Drawing.Color.White;
            this.btnDanhSachBan.Image = ((System.Drawing.Image)(resources.GetObject("btnDanhSachBan.Image")));
            this.btnDanhSachBan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDanhSachBan.Location = new System.Drawing.Point(0, 118);
            this.btnDanhSachBan.Name = "btnDanhSachBan";
            this.btnDanhSachBan.Size = new System.Drawing.Size(235, 54);
            this.btnDanhSachBan.TabIndex = 14;
            this.btnDanhSachBan.Text = "   Quét danh sách bạn";
            this.btnDanhSachBan.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDanhSachBan.UseVisualStyleBackColor = true;
            this.btnDanhSachBan.Click += new System.EventHandler(this.btnDanhSachBan_Click);
            // 
            // btnPost
            // 
            this.btnPost.Enabled = false;
            this.btnPost.FlatAppearance.BorderSize = 0;
            this.btnPost.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPost.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPost.ForeColor = System.Drawing.Color.White;
            this.btnPost.Image = ((System.Drawing.Image)(resources.GetObject("btnPost.Image")));
            this.btnPost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPost.Location = new System.Drawing.Point(0, 336);
            this.btnPost.Name = "btnPost";
            this.btnPost.Size = new System.Drawing.Size(235, 54);
            this.btnPost.TabIndex = 6;
            this.btnPost.Text = "   Quét bài viết page";
            this.btnPost.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPost.UseVisualStyleBackColor = true;
            this.btnPost.Click += new System.EventHandler(this.btnPost_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(73, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "FACEBOOK";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(67, 48);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // btnComment
            // 
            this.btnComment.Enabled = false;
            this.btnComment.FlatAppearance.BorderSize = 0;
            this.btnComment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnComment.ForeColor = System.Drawing.Color.White;
            this.btnComment.Image = ((System.Drawing.Image)(resources.GetObject("btnComment.Image")));
            this.btnComment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnComment.Location = new System.Drawing.Point(0, 282);
            this.btnComment.Name = "btnComment";
            this.btnComment.Size = new System.Drawing.Size(235, 54);
            this.btnComment.TabIndex = 7;
            this.btnComment.Text = "   Quét comment";
            this.btnComment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnComment.UseVisualStyleBackColor = true;
            this.btnComment.Click += new System.EventHandler(this.btnComment_Click);
            // 
            // btnLike
            // 
            this.btnLike.Enabled = false;
            this.btnLike.FlatAppearance.BorderSize = 0;
            this.btnLike.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLike.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLike.ForeColor = System.Drawing.Color.White;
            this.btnLike.Image = ((System.Drawing.Image)(resources.GetObject("btnLike.Image")));
            this.btnLike.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLike.Location = new System.Drawing.Point(0, 228);
            this.btnLike.Name = "btnLike";
            this.btnLike.Size = new System.Drawing.Size(235, 54);
            this.btnLike.TabIndex = 8;
            this.btnLike.Text = "   Quét like";
            this.btnLike.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLike.UseVisualStyleBackColor = true;
            this.btnLike.Click += new System.EventHandler(this.btnLike_Click);
            // 
            // btnThanhVien
            // 
            this.btnThanhVien.Enabled = false;
            this.btnThanhVien.FlatAppearance.BorderSize = 0;
            this.btnThanhVien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThanhVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThanhVien.ForeColor = System.Drawing.Color.White;
            this.btnThanhVien.Image = ((System.Drawing.Image)(resources.GetObject("btnThanhVien.Image")));
            this.btnThanhVien.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnThanhVien.Location = new System.Drawing.Point(0, 173);
            this.btnThanhVien.Name = "btnThanhVien";
            this.btnThanhVien.Size = new System.Drawing.Size(235, 54);
            this.btnThanhVien.TabIndex = 9;
            this.btnThanhVien.Text = "   Quét nhóm";
            this.btnThanhVien.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnThanhVien.UseVisualStyleBackColor = true;
            this.btnThanhVien.Click += new System.EventHandler(this.btnThanhVien_Click);
            // 
            // btnTrangChu
            // 
            this.btnTrangChu.BackColor = System.Drawing.Color.Crimson;
            this.btnTrangChu.FlatAppearance.BorderSize = 0;
            this.btnTrangChu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrangChu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrangChu.ForeColor = System.Drawing.Color.White;
            this.btnTrangChu.Image = ((System.Drawing.Image)(resources.GetObject("btnTrangChu.Image")));
            this.btnTrangChu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTrangChu.Location = new System.Drawing.Point(0, 64);
            this.btnTrangChu.Name = "btnTrangChu";
            this.btnTrangChu.Size = new System.Drawing.Size(235, 54);
            this.btnTrangChu.TabIndex = 11;
            this.btnTrangChu.Text = "   Trang chủ";
            this.btnTrangChu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTrangChu.UseVisualStyleBackColor = false;
            this.btnTrangChu.Click += new System.EventHandler(this.btnTrangChu_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // pnHomeLogin
            // 
            this.pnHomeLogin.Controls.Add(this.label4);
            this.pnHomeLogin.Controls.Add(this.label1);
            this.pnHomeLogin.Controls.Add(this.txtPass);
            this.pnHomeLogin.Controls.Add(this.txtEmail);
            this.pnHomeLogin.Controls.Add(this.btnLogin);
            this.pnHomeLogin.Controls.Add(this.txtAccessToken);
            this.pnHomeLogin.Location = new System.Drawing.Point(236, 72);
            this.pnHomeLogin.Name = "pnHomeLogin";
            this.pnHomeLogin.Size = new System.Drawing.Size(885, 190);
            this.pnHomeLogin.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Mật khẩu FB";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Email FB";
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(111, 46);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(184, 20);
            this.txtPass.TabIndex = 7;
            this.txtPass.UseSystemPasswordChar = true;
            this.txtPass.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPass_KeyUp);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(110, 11);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(185, 20);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEmail_KeyUp);
            // 
            // btnQuet1UID
            // 
            this.btnQuet1UID.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnQuet1UID.FlatAppearance.BorderSize = 0;
            this.btnQuet1UID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuet1UID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuet1UID.ForeColor = System.Drawing.Color.White;
            this.btnQuet1UID.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuet1UID.Location = new System.Drawing.Point(0, 1);
            this.btnQuet1UID.Name = "btnQuet1UID";
            this.btnQuet1UID.Size = new System.Drawing.Size(114, 63);
            this.btnQuet1UID.TabIndex = 12;
            this.btnQuet1UID.Text = "Quét một UID";
            this.btnQuet1UID.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuet1UID.UseVisualStyleBackColor = true;
            this.btnQuet1UID.Click += new System.EventHandler(this.btnQuet1UID_Click);
            // 
            // btnQuetTepUID
            // 
            this.btnQuetTepUID.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnQuetTepUID.FlatAppearance.BorderSize = 0;
            this.btnQuetTepUID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuetTepUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuetTepUID.ForeColor = System.Drawing.Color.White;
            this.btnQuetTepUID.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuetTepUID.Location = new System.Drawing.Point(120, 3);
            this.btnQuetTepUID.Name = "btnQuetTepUID";
            this.btnQuetTepUID.Size = new System.Drawing.Size(121, 59);
            this.btnQuetTepUID.TabIndex = 13;
            this.btnQuetTepUID.Text = "Quét tệp UID";
            this.btnQuetTepUID.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnQuetTepUID.UseVisualStyleBackColor = true;
            this.btnQuetTepUID.Click += new System.EventHandler(this.btnQuetTepUID_Click);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(247, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(119, 59);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Lưu danh sách";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(372, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(125, 59);
            this.btnDelete.TabIndex = 15;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(503, 2);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(115, 59);
            this.btnLogout.TabIndex = 16;
            this.btnLogout.Text = "Đổi tài khoản";
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // pnQuetDSBanHeader
            // 
            this.pnQuetDSBanHeader.BackColor = System.Drawing.Color.SteelBlue;
            this.pnQuetDSBanHeader.Controls.Add(this.btnLuuDSUID);
            this.pnQuetDSBanHeader.Controls.Add(this.nmGioiHan);
            this.pnQuetDSBanHeader.Controls.Add(this.lblGioihan);
            this.pnQuetDSBanHeader.Controls.Add(this.btnLogout);
            this.pnQuetDSBanHeader.Controls.Add(this.btnDelete);
            this.pnQuetDSBanHeader.Controls.Add(this.btnSave);
            this.pnQuetDSBanHeader.Controls.Add(this.btnQuetTepUID);
            this.pnQuetDSBanHeader.Controls.Add(this.btnQuet1UID);
            this.pnQuetDSBanHeader.Location = new System.Drawing.Point(236, 1);
            this.pnQuetDSBanHeader.Name = "pnQuetDSBanHeader";
            this.pnQuetDSBanHeader.Size = new System.Drawing.Size(885, 64);
            this.pnQuetDSBanHeader.TabIndex = 9;
            // 
            // btnLuuDSUID
            // 
            this.btnLuuDSUID.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnLuuDSUID.FlatAppearance.BorderSize = 0;
            this.btnLuuDSUID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLuuDSUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuDSUID.ForeColor = System.Drawing.Color.White;
            this.btnLuuDSUID.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLuuDSUID.Location = new System.Drawing.Point(620, 2);
            this.btnLuuDSUID.Name = "btnLuuDSUID";
            this.btnLuuDSUID.Size = new System.Drawing.Size(115, 59);
            this.btnLuuDSUID.TabIndex = 22;
            this.btnLuuDSUID.Text = "Lưu DS UID";
            this.btnLuuDSUID.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLuuDSUID.UseVisualStyleBackColor = true;
            this.btnLuuDSUID.Click += new System.EventHandler(this.btnLuuDSUID_Click);
            // 
            // nmGioiHan
            // 
            this.nmGioiHan.Location = new System.Drawing.Point(741, 33);
            this.nmGioiHan.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nmGioiHan.Name = "nmGioiHan";
            this.nmGioiHan.Size = new System.Drawing.Size(90, 20);
            this.nmGioiHan.TabIndex = 21;
            this.nmGioiHan.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nmGioiHan.Visible = false;
            // 
            // lblGioihan
            // 
            this.lblGioihan.AutoSize = true;
            this.lblGioihan.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGioihan.ForeColor = System.Drawing.Color.White;
            this.lblGioihan.Location = new System.Drawing.Point(752, 8);
            this.lblGioihan.Name = "lblGioihan";
            this.lblGioihan.Size = new System.Drawing.Size(61, 17);
            this.lblGioihan.TabIndex = 20;
            this.lblGioihan.Text = "Giới hạn";
            this.lblGioihan.Visible = false;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(738, 11);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(44, 13);
            this.lblTotal.TabIndex = 14;
            this.lblTotal.Text = "Tổng: ";
            // 
            // dgDanhSach
            // 
            this.dgDanhSach.BackgroundColor = System.Drawing.Color.White;
            this.dgDanhSach.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDanhSach.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgDanhSach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDanhSach.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgDanhSach.Location = new System.Drawing.Point(3, 34);
            this.dgDanhSach.Name = "dgDanhSach";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDanhSach.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgDanhSach.Size = new System.Drawing.Size(882, 423);
            this.dgDanhSach.TabIndex = 2;
            // 
            // txtUID
            // 
            this.txtUID.Location = new System.Drawing.Point(113, 8);
            this.txtUID.Name = "txtUID";
            this.txtUID.Size = new System.Drawing.Size(622, 20);
            this.txtUID.TabIndex = 3;
            this.txtUID.Text = "me";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(5, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nhập ID ở đây";
            // 
            // pnListData
            // 
            this.pnListData.Controls.Add(this.label5);
            this.pnListData.Controls.Add(this.lblTotal);
            this.pnListData.Controls.Add(this.txtUID);
            this.pnListData.Controls.Add(this.dgDanhSach);
            this.pnListData.Location = new System.Drawing.Point(236, 64);
            this.pnListData.Name = "pnListData";
            this.pnListData.Size = new System.Drawing.Size(885, 460);
            this.pnListData.TabIndex = 15;
            // 
            // frmFBUID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(1125, 524);
            this.Controls.Add(this.pnListData);
            this.Controls.Add(this.pnQuetDSBanHeader);
            this.Controls.Add(this.pnHomeLogin);
            this.Controls.Add(this.pnMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmFBUID";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FACEBOOK UID - Ứng dụng kết nối - Giải pháp maketing hiệu quả";
            this.pnMenu.ResumeLayout(false);
            this.pnMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.pnHomeLogin.ResumeLayout(false);
            this.pnHomeLogin.PerformLayout();
            this.pnQuetDSBanHeader.ResumeLayout(false);
            this.pnQuetDSBanHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmGioiHan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDanhSach)).EndInit();
            this.pnListData.ResumeLayout(false);
            this.pnListData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtAccessToken;
        private System.Windows.Forms.Panel pnMenu;
        private System.Windows.Forms.Button btnPost;
        private System.Windows.Forms.Button btnComment;
        private System.Windows.Forms.Button btnLike;
        private System.Windows.Forms.Button btnThanhVien;
        private System.Windows.Forms.Button btnTrangChu;
        private System.Windows.Forms.Button btnDanhSachBan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Panel pnHomeLogin;
        private System.Windows.Forms.Panel pnQuetDSBanHeader;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnQuetTepUID;
        private System.Windows.Forms.Button btnQuet1UID;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblGioihan;
        private System.Windows.Forms.NumericUpDown nmGioiHan;
        private System.Windows.Forms.Button btnLuuDSUID;
        private System.Windows.Forms.Button btnLikePage;
        private System.Windows.Forms.Panel pnListData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUID;
        private System.Windows.Forms.DataGridView dgDanhSach;
    }
}

