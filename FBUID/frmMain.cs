﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Facebook;
using Newtonsoft.Json.Linq;
using FBUID.Common;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Net.Http;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Diagnostics;
using System.Security.Cryptography;
using FBUID.MeFriend;
using FBUID.MeGroup;
using FBUID.MeLikePost;
using FBUID.MePost;
using FBUID.MeCommentPost;
using FBUID.MeLikePage;

namespace FBUID
{
    public partial class frmFBUID : Form
    {
        FacebookClient fbClient = new FacebookClient();
        private const int PgSize = 20;
        public static int totalRecords = 0;
        List<FacebookModel> lstConst = new List<FacebookModel>();
        BindingSource bs = new BindingSource();
        private string textFile = "";
        FriendHandler handlerFr = new FriendHandler();
        GroupHandler handlerGr = new GroupHandler();
        LikeHandler handlerLike = new LikeHandler();
        PostHandler handlerPost = new PostHandler();
        LikePageHandler handlerLikePage = new LikePageHandler();
        CommentPostHandler handerCmt = new CommentPostHandler();
        List<ListDisplayGridview> lstFrs = new List<ListDisplayGridview>();
        List<ListDisplayGridviewGroup> lstMbs = new List<ListDisplayGridviewGroup>();
        List<ListDisplayGridviewLike> lstUsLike = new List<ListDisplayGridviewLike>();
        List<ListDisplayGridviewPost> lstUsPost = new List<ListDisplayGridviewPost>();
        List<ListDisplayGridviewCMT> lstCmtPost = new List<ListDisplayGridviewCMT>();
        List<ListDisplayGridviewLikePage> lstLikePage = new List<ListDisplayGridviewLikePage>();
        public string idUser = "";
        private int menuchoose = 1;

        public frmFBUID()
        {
            InitializeComponent();
            pnHomeLogin.Visible = true;
            pnQuetDSBanHeader.Visible = false;
            pnListData.Visible = false;
            this.ActiveControl = txtEmail;
            //txtEmail.Text = "nguyendanhthien90@gmail.com";
            //txtPass.Text = "Ht12021990";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string email = txtEmail.Text;
            string pass = txtPass.Text;
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(pass))
                Login(email, pass);
            else
                MessageBox.Show("Bạn chưa nhập đầy đủ thông tin đăng nhập!");
        }

        private void btnTrangChu_Click(object sender, EventArgs e)
        {
            //set display control properties
            pnHomeLogin.Visible = true;
            pnListData.Visible = false;
            pnQuetDSBanHeader.Visible = false;
            //Set active
            btnTrangChu.BackColor = Color.Crimson;
            //Set InActive
            btnDanhSachBan.BackColor = Color.SteelBlue;
            btnThanhVien.BackColor = Color.SteelBlue;
            btnLike.BackColor = Color.SteelBlue;
            btnComment.BackColor = Color.SteelBlue;
            btnPost.BackColor = Color.SteelBlue;
            menuchoose = 0;
        }

        private void btnDanhSachBan_Click(object sender, EventArgs e)
        {
            //set display control properties
            pnHomeLogin.Visible = false;
            pnListData.Visible = true;
            pnQuetDSBanHeader.Visible = true;
            lblGioihan.Visible = false;
            nmGioiHan.Visible = false;
            btnLuuDSUID.Visible = true;

            //Set active            
            btnDanhSachBan.BackColor = Color.Crimson;
            //Set InActive
            btnTrangChu.BackColor = Color.SteelBlue;
            btnThanhVien.BackColor = Color.SteelBlue;
            btnLike.BackColor = Color.SteelBlue;
            btnComment.BackColor = Color.SteelBlue;
            btnPost.BackColor = Color.SteelBlue;
            btnLikePage.BackColor = Color.SteelBlue;

            //Set text cho menu ngang
            btnQuet1UID.Text = "Quét một UID";
            btnQuetTepUID.Text = "Quét tệp UID";
            btnSave.Text = "Lưu danh sách";
            btnDelete.Text = "Xóa danh sách";
            btnLogout.Text = "Đổi tài khoản";

            //clear grid
            dgDanhSach.DataSource = null;
            txtUID.Text = "me";

            //define menu choose
            menuchoose = Constant.QUET_DANH_SACH_BAN;
        }

        private void btnThanhVien_Click(object sender, EventArgs e)
        {
            //set display control properties
            pnHomeLogin.Visible = false;
            pnListData.Visible = true;
            pnQuetDSBanHeader.Visible = true;
            lblGioihan.Visible = false;
            nmGioiHan.Visible = false;
            btnLuuDSUID.Visible = true;

            //Set active            
            btnThanhVien.BackColor = Color.Crimson;
            //Set InActive
            btnTrangChu.BackColor = Color.SteelBlue;
            btnDanhSachBan.BackColor = Color.SteelBlue;
            btnLike.BackColor = Color.SteelBlue;
            btnComment.BackColor = Color.SteelBlue;
            btnPost.BackColor = Color.SteelBlue;
            btnLikePage.BackColor = Color.SteelBlue;

            //Set text cho menu ngang
            btnQuet1UID.Text = "Quét một GID";
            btnQuetTepUID.Text = "Quét tệp GID";
            btnSave.Text = "Lưu danh sách";
            btnDelete.Text = "Xóa danh sách";
            btnLogout.Text = "Lọc  trùng";

            //clear grid
            dgDanhSach.DataSource = null;
            txtUID.Text = "Nhập Group ID";

            //define menu choose
            menuchoose = Constant.QUET_THANH_VIEN_NHOM;
        }

        private void btnLike_Click(object sender, EventArgs e)
        {
            //set display control properties
            pnHomeLogin.Visible = false;
            pnListData.Visible = true;
            pnQuetDSBanHeader.Visible = true;
            lblGioihan.Visible = false;
            nmGioiHan.Visible = false;
            btnLuuDSUID.Visible = false;

            //Set active            
            btnLike.BackColor = Color.Crimson;
            //Set InActive
            btnTrangChu.BackColor = Color.SteelBlue;
            btnDanhSachBan.BackColor = Color.SteelBlue;
            btnThanhVien.BackColor = Color.SteelBlue;
            btnComment.BackColor = Color.SteelBlue;
            btnPost.BackColor = Color.SteelBlue;
            btnLikePage.BackColor = Color.SteelBlue;

            //Set text cho menu ngang
            btnQuet1UID.Text = "Quét một ID";
            btnQuetTepUID.Text = "Quét tệp ID";
            btnSave.Text = "Lưu danh sách";
            btnDelete.Text = "Xóa danh sách";
            btnLogout.Text = "Lọc trùng";

            //clear grid
            dgDanhSach.DataSource = null;
            txtUID.Text = "Nhập Post ID";

            //define menu choose
            menuchoose = Constant.QUET_LIKE;
        }

        private void btnComment_Click(object sender, EventArgs e)
        {
            //set display control properties
            pnHomeLogin.Visible = false;
            pnListData.Visible = true;
            btnDelete.Visible = true;
            pnQuetDSBanHeader.Visible = true;
            lblGioihan.Visible = false;
            nmGioiHan.Visible = false;
            btnLuuDSUID.Visible = true;

            //Set active            
            btnComment.BackColor = Color.Crimson;
            //Set InActive
            btnTrangChu.BackColor = Color.SteelBlue;
            btnDanhSachBan.BackColor = Color.SteelBlue;
            btnThanhVien.BackColor = Color.SteelBlue;
            btnLike.BackColor = Color.SteelBlue;
            btnPost.BackColor = Color.SteelBlue;
            btnLikePage.BackColor = Color.SteelBlue;

            //Set text cho menu ngang
            btnQuet1UID.Text = "Quét một ID";
            btnQuetTepUID.Text = "Quét tệp ID";
            btnSave.Text = "Lưu danh sách";
            btnDelete.Text = "Xóa danh sách";
            btnLogout.Text = "Lọc trùng";

            //clear grid
            dgDanhSach.DataSource = null;
            txtUID.Text = "Nhập Post ID";

            //define menu choose
            menuchoose = Constant.QUET_COMMENT;
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            //set display control properties
            pnHomeLogin.Visible = false;
            pnListData.Visible = true;
            pnQuetDSBanHeader.Visible = true;
            lblGioihan.Visible = true;
            nmGioiHan.Visible = true;
            btnLuuDSUID.Visible = false;

            //Set active            
            btnPost.BackColor = Color.Crimson;
            //Set InActive
            btnTrangChu.BackColor = Color.SteelBlue;
            btnDanhSachBan.BackColor = Color.SteelBlue;
            btnThanhVien.BackColor = Color.SteelBlue;
            btnLike.BackColor = Color.SteelBlue;
            btnComment.BackColor = Color.SteelBlue;
            btnLikePage.BackColor = Color.SteelBlue;

            //Set text cho menu ngang
            btnQuet1UID.Text = "Quét một ID";
            btnQuetTepUID.Text = "Quét tệp ID";
            btnSave.Text = "Lưu DS";
            btnDelete.Text = "Xóa DS";
            btnLogout.Text = "Lọc trùng";

            //clear grid
            dgDanhSach.DataSource = null;
            txtUID.Text = "Nhập Page ID";

            //define menu choose
            menuchoose = Constant.QUET_BAI_VIET;
        }

        /// <summary>
        /// Sự kiện lấy về danh sách bạn bè theo access token
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuet1UID_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> lstUid = new List<string>();
                lstUid.Add(txtUID.Text.Trim());

                //kiem tra menu duoc chon de xu ly
                if (menuchoose == Constant.QUET_DANH_SACH_BAN)
                {
                    lstFrs = handlerFr.GetListFriend(lstUid, txtAccessToken.Text);
                    dgDanhSach.DataSource = lstFrs;
                    lblTotal.Text = "Tổng: " + lstFrs.Count();
                }
                else if (menuchoose == Constant.QUET_THANH_VIEN_NHOM)
                {
                    lstMbs = handlerGr.GetListMember(lstUid, txtAccessToken.Text);
                    dgDanhSach.DataSource = lstMbs;
                    lblTotal.Text = "Tổng: " + lstMbs.Count();
                }
                else if (menuchoose == Constant.QUET_LIKE)
                {
                    lstUsLike = handlerLike.GetListUserLikesPost(lstUid, txtAccessToken.Text);
                    dgDanhSach.DataSource = lstUsLike;
                    lblTotal.Text = "Tổng: " + lstUsLike.Count();
                }
                else if (menuchoose == Constant.QUET_BAI_VIET)
                {
                    lstUsPost = handlerPost.GetListUserPost(lstUid, txtAccessToken.Text, nmGioiHan.Value.ToString());
                    dgDanhSach.DataSource = lstUsPost;
                    lblTotal.Text = "Tổng: " + lstUsPost.Count();
                }
                else if (menuchoose == Constant.QUET_COMMENT)
                {
                    lstCmtPost = handerCmt.GetListCommentPost(lstUid, txtAccessToken.Text, idUser);
                    dgDanhSach.DataSource = lstCmtPost;
                    lblTotal.Text = "Tổng: " + lstCmtPost.Count();
                }
                else if (menuchoose == Constant.QUET_LIKE_PAGE)
                {
                    lstLikePage = handlerLikePage.GetListUserLikePage(lstUid, txtAccessToken.Text);
                    dgDanhSach.DataSource = lstLikePage;
                    lblTotal.Text = "Tổng: " + lstLikePage.Count();
                }

                DataGridViewColumn column = dgDanhSach.Columns[0];
                column.Width = 40;
                DataGridViewColumn column2 = dgDanhSach.Columns[2];
                column2.Width = 200;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// sự kiện đăng xuất đổi tài khoản đăng nhập
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                //kiem tra menu duoc cho de xu ly
                if (menuchoose == Constant.QUET_DANH_SACH_BAN)
                {
                    pnHomeLogin.Visible = true;
                    pnQuetDSBanHeader.Visible = false;
                    pnListData.Visible = false;
                    List<ListDisplayGridview> dataClear = new List<ListDisplayGridview>();
                    dgDanhSach.DataSource = dataClear;

                    btnTrangChu.BackColor = Color.Crimson;
                    //Set InActive
                    btnDanhSachBan.BackColor = Color.SteelBlue;
                    btnThanhVien.BackColor = Color.SteelBlue;
                    btnLike.BackColor = Color.SteelBlue;
                    btnComment.BackColor = Color.SteelBlue;
                    btnPost.BackColor = Color.SteelBlue;

                    btnTrangChu.Enabled = true;
                    btnDanhSachBan.Enabled = false;
                    btnThanhVien.Enabled = false;
                    btnLike.Enabled = false;
                    btnComment.Enabled = false;
                    btnPost.Enabled = false;

                    txtAccessToken.Text = "Token hiển thị ở đây...";
                    txtEmail.Text = "";
                    txtPass.Text = "";
                    txtEmail.Focus();
                }
                //else if (menuchoose == Constant.QUET_THANH_VIEN_NHOM || menuchoose == Constant.QUET_LIKE || menuchoose == Constant.QUET_BAI_VIET || menuchoose == Constant.QUET_LIKE_PAGE)
                else
                {
                    List<ListDisplayGridviewGroup> lstGr = new List<ListDisplayGridviewGroup>();
                    if (lstMbs != null && lstMbs.Count > 0)
                    {
                        lstGr = lstMbs.Distinct().ToList();
                        dgDanhSach.DataSource = lstGr;
                    }
                }
                //else if (menuchoose == Constant.QUET_COMMENT)
                //{
                //    if (lstCmtPost.Count > 0)
                //    {
                //        lstCmtPost = handerCmt.GetListCommentPostKhongTrung(lstCmtPost);
                //        dgDanhSach.DataSource = lstCmtPost;
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region luu danh sach duoi dang excel
        /// <summary>
        /// Copy datagridview to clipboard as object
        /// </summary>
        private void copyAlltoClipboard()
        {
            try
            {
                dgDanhSach.SelectAll();
                DataObject dataObj = dgDanhSach.GetClipboardContent();
                if (dataObj != null)
                    Clipboard.SetDataObject(dataObj);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void copyAllIdtoClipboard(List<string> lstId)
        {
            try
            {
                //dgDanhSach.SelectAll();
                DataObject dataObj = dgDanhSach.GetClipboardContent();
                if (dataObj != null)
                    Clipboard.SetDataObject(lstId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                copyAlltoClipboard();
                Excel.Application xlexcel;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                xlexcel = new Microsoft.Office.Interop.Excel.Application();
                xlexcel.Visible = true;
                xlWorkBook = xlexcel.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[1, 1];
                CR.Select();
                xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void btnQuetTepUID_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Title = "Open Text File";
                dialog.Filter = "TXT files|*.txt";
                dialog.InitialDirectory = @"C:\";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string file = Path.GetFileName(dialog.FileName);
                    string path = Path.GetDirectoryName(dialog.FileName) ?? "\\";
                    path = path + "\\" + file;
                    //string path2 = Path.GetDirectoryName(file);
                    //FileStream fsrw = new FileStream(path, FileMode.Open, FileAccess.ReadWrite);
                    StreamReader reader = new StreamReader(path);
                    textFile = System.IO.File.ReadAllText(path);

                    if (!string.IsNullOrEmpty(textFile) && textFile.Contains(","))
                    {
                        List<string> lstUID = textFile.Split(',').ToList();
                        if (lstUID != null && lstUID.Count > 0)
                        {
                            if (menuchoose == Constant.QUET_DANH_SACH_BAN)
                                handlerFr.GetListFriend(lstUID, txtAccessToken.Text);
                            else if (menuchoose == Constant.QUET_THANH_VIEN_NHOM)
                                handlerGr.GetListMember(lstUID, txtAccessToken.Text);
                            else if (menuchoose == Constant.QUET_BAI_VIET)
                                handlerPost.GetListUserPost(lstUID, txtAccessToken.Text, nmGioiHan.Value.ToString());
                            else if (menuchoose == Constant.QUET_COMMENT)
                                lstCmtPost = handerCmt.GetListCommentPost(lstUID, txtAccessToken.Text, idUser);
                            else if (menuchoose == Constant.QUET_LIKE_PAGE)
                                lstLikePage = handlerLikePage.GetListUserLikePage(lstUID, txtAccessToken.Text);
                            dgDanhSach.DataSource = lstCmtPost;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region Event xoa danh sach
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                dgDanhSach.DataSource = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void btnLuuDSUID_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> lstId = new List<string>();
                if (menuchoose == Constant.QUET_THANH_VIEN_NHOM)
                {
                    if (lstMbs != null && lstMbs.Count > 0)
                    {
                        lstId = lstMbs.Select(x => x.ID).ToList();
                    }
                }
                if (menuchoose == Constant.QUET_DANH_SACH_BAN)
                {
                    if (lstFrs != null && lstFrs.Count > 0)
                    {
                        lstId = lstFrs.Select(x => x.ID).ToList();
                    }
                }
                if (menuchoose == Constant.QUET_COMMENT)
                {
                    if (lstCmtPost != null && lstCmtPost.Count > 0)
                    {
                        lstId = lstCmtPost.Select(x => x.ID).ToList();
                    }
                }

                if (lstId.Count > 0)
                {
                    copyAllIdtoClipboard(lstId);
                    Excel.Application xlexcel;
                    Excel.Workbook xlWorkBook;
                    Excel.Worksheet xlWorkSheet;
                    object misValue = System.Reflection.Missing.Value;
                    xlexcel = new Microsoft.Office.Interop.Excel.Application();
                    xlexcel.Visible = true;
                    xlWorkBook = xlexcel.Workbooks.Add(misValue);
                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[1, 1];
                    CR.Select();
                    xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
                }
                else
                {
                    MessageBox.Show("Chưa có dữ liệu được tìm thấy!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnLikePage_Click(object sender, EventArgs e)
        {
            //set display control properties
            pnHomeLogin.Visible = false;
            pnListData.Visible = true;
            pnQuetDSBanHeader.Visible = true;
            lblGioihan.Visible = false;
            nmGioiHan.Visible = false;
            btnLuuDSUID.Visible = false;

            //Set active    
            btnLikePage.BackColor = Color.Crimson;
            //Set InActive
            btnPost.BackColor = Color.SteelBlue;
            btnTrangChu.BackColor = Color.SteelBlue;
            btnDanhSachBan.BackColor = Color.SteelBlue;
            btnThanhVien.BackColor = Color.SteelBlue;
            btnLike.BackColor = Color.SteelBlue;
            btnComment.BackColor = Color.SteelBlue;

            //Set text cho menu ngang
            btnQuet1UID.Text = "Quét một ID";
            btnQuetTepUID.Text = "Quét tệp ID";
            btnSave.Text = "Lưu DS";
            btnDelete.Text = "Xóa DS";
            btnLogout.Text = "Lọc trùng";

            //clear grid
            dgDanhSach.DataSource = null;
            txtUID.Text = "Nhập Page ID";

            //define menu choose
            menuchoose = Constant.QUET_LIKE_PAGE;
        }

        private void txtPass_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    string email = txtEmail.Text;
                    string pass = txtPass.Text;
                    if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(pass))
                        Login(email, pass);
                    else
                        MessageBox.Show("Bạn chưa nhập đầy đủ thông tin đăng nhập!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtEmail_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    string email = txtEmail.Text;
                    string pass = txtPass.Text;
                    if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(pass))
                        Login(email, pass);
                    else
                        MessageBox.Show("Bạn chưa nhập đầy đủ thông tin đăng nhập!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region function login
        private void Login(string email, string pass)
        {
            try
            {

                string signKey = Common.Constant.SIGN_KEY;
                string apiKey = Common.Constant.API_KEY;

                InputGetToken query = new InputGetToken()
                {
                    api_key = apiKey,
                    email = email,
                    format = "JSON",
                    locale = "vi_vn",
                    method = "auth.login",
                    password = pass,
                    return_ssl_resources = "0",
                    v = "1.0"
                };

                string sig = "";
                sig += "api_key=" + query.api_key + "&email=" + query.email + "&format=" + query.format + "&locale=" + query.locale + "&method=" + query.method + "&password=" + query.password + "&return_ssl_resources=" + query.return_ssl_resources + "&v=" + query.v;

                string inputMd5 = sig.Replace("&", "").Trim() + "" + signKey;
                string md5Sign = Common.CommonClass.MD5Hash(inputMd5);

                sig += "&sig=" + md5Sign;

                string url = @"https://api.facebook.com/restserver.php?" + sig;
                using (var client = new HttpClient())
                {
                    HttpResponseMessage a = client.GetAsync(url).Result;
                    var b = a.Content.ReadAsStringAsync();
                    OutputGetToken c = JsonConvert.DeserializeObject<OutputGetToken>(b.Result);
                    if (string.IsNullOrEmpty(c.access_token))
                        MessageBox.Show("Thông tin tài khoản chưa chính xác hoặc tạm thời bị khóa. Vui lòng kiểm tra lại!");
                    else
                    {
                        txtAccessToken.Text = c.access_token;
                        idUser = c.uid;
                        pnHomeLogin.Visible = false;
                        pnListData.Visible = true;
                        pnQuetDSBanHeader.Visible = true;

                        //Set active            
                        btnDanhSachBan.BackColor = Color.Crimson;
                        //Set InActive
                        btnTrangChu.BackColor = Color.SteelBlue;
                        btnThanhVien.BackColor = Color.SteelBlue;
                        btnLike.BackColor = Color.SteelBlue;
                        btnComment.BackColor = Color.SteelBlue;
                        btnPost.BackColor = Color.SteelBlue;
                        //Set enable
                        btnTrangChu.Enabled = true;
                        btnDanhSachBan.Enabled = true;
                        btnThanhVien.Enabled = true;
                        btnLike.Enabled = true;
                        btnComment.Enabled = true;
                        btnPost.Enabled = true;
                        btnLikePage.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
